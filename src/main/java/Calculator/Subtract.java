
package Calculator;

public class Subtract extends Calculator {
    
    public Subtract( double operand1, double operand2){
        super(operand1, operand2);
    }
    
    public double calculate(){
        return operand1 - operand2;
    }
}
