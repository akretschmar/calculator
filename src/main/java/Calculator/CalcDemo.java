
package Calculator;

/**
 *
 * @author akret
 */
public class CalcDemo {

    public static void main(String[] args) {
        
       Add add = new Add(1,2);
       Subtract subtract = new Subtract(1,2);
       
        System.out.println(add.calculate());
        System.out.println(subtract.calculate());
    }
    
}
