
package Calculator;

public abstract class Calculator {
    protected double operand1;
    protected double operand2;
    
    protected Calculator(){}
    
    protected Calculator(double operand1, double operand2){
        this.operand1 = operand1;
        this.operand2 = operand2;
    }
    
    public void setOperand1(double operand1){
        this.operand1 = operand1;
    }
    
    public void setOperand2(double operand2){
        this.operand2 = operand2;
    }
    public abstract double calculate();
}
